---
title: About this page
subtitle: -navigating into/through academia- do I want to pursue a PhD programme?!
comments: false
---

I am Anna, train-travel and tiny-cinema enthusiast, B.Sc. in Psychology and about to finish my Master programme in Cognitive Neuroscience. Me and friends of mine made a multitude of different experiences on our way through (similar) studies, and now want to share some of the impressions. This seemed like a feasible way to collect:

- importance of mental health, setting personal priorities, receiving and selection positions with good supervision structure
- dealing with rejections
- actual applications, that were sent to job offerings, graduate school programmes, advertised research assistantships or laboratory technicians and other jobs
- cv-related questions
- getting to know different country's admission and university systems 
- behaving and networking on conferences/workshops (and how it can be fun)
- stories of personal experiences and struggle
- utility-tools for the process, such as LaTeX, git(lab/hub)
- community-related tipps for establishing a small, initial network with e.g. twitter, researchgate
- diversity in (neuro-)science, citing culture, peers, teams 
- ethical considerations on *how* we want to do research and (maybe) short essays about it


### but...why? & why in this form?
Honestly, I felt pretty lost trying to understand this world of academia, its customs, its means of communication (lets be real: others use twitter for puppy gifs and entertainment, but: its science too). I would like to put down some of the random social knowledge I, and others, seem to have gathered. Of course, there are so many paths and ways to explore, this could maybe be a starting point. I would have loved to see more of diversity in narratives about becoming a 'Scientist' during my Bachelors, so illustrating it more closely is important to me. 

